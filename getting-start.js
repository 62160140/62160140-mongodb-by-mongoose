const mongoose =  require('mongoose')
mongoose.connect('mongodb://localhost:27017/example')

//todo สร้าง  Class , Schema
const kittySchema = new mongoose.Schema({
  name:String
})

//todo สร้าง function ให้ kitty
kittySchema.methods.speak = function speak(){
  const greeting = this.name ? "Meow name is "+this.name
  : "I dont have name."
  console.log(greeting)
}

//todo สร้าง model
const Kitten = mongoose.model('Kitten',kittySchema)

//todo สร้างObject โดยใช้ model Kitten 
const silence = new Kitten({name:'Silence'})
silence.speak()
/* ---- เกิด Collection Kitten แล้ว แต่ยังไม่มีข้อมูล --- */

/* ------------------- ต้อง save ก่อน(แบบ Promise) ------------------- */
silence.save().then((result)=>console.log(result)).catch((err)=>console.log(err))


/* ------------------ save(แบบCallback) ----------------- */
const fluffy = new Kitten({name:'fluffy'})
fluffy.speak()
fluffy.save(function(err,result){
  if(err){
    console.log(err)
  }else{
    console.log(result)
  }
})
/* ------------------- save(แบบ Async) ------------------ */
async function saveCat(name){
  const cat = new Kitten({name:name})
  try{
  const result = await cat.save()
  console.log(result)
  return result
  }catch(e){
    console.log(e)
  }
}

saveCat('Ta').then((result)=>console.log(result))