//* id ไม่ต้องสร้าง mongo gen ให้

//todo ดึง lib mongoose มา
const mongoose = require('mongoose')

//todo เอาตัว Schema มาเพื่อง่ายขึ้น
const {Schema} = mongoose


/* ---------------- สร้าง Schema , Class ---------------- */
//todo จากตอนแรก Schema.mongoose เป็น Schema 
const buildingSchema  = Schema({
  name:{type:String},
  floor:Number,
  // !ref 
  // !default value
  rooms:[{type: Schema.Types.ObjectId, ref:'Room' ,default:[]}]
})

//! สร้าง model 
//! เวลาใครจะเรียกใช้เรียกผ่าน Building
module.exports = mongoose.model('Building', buildingSchema)