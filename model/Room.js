//* id ไม่ต้องสร้าง mongo gen ให้

//todo ดึง lib mongoose มา
const mongoose = require('mongoose')

//todo เอาตัว Schema มาเพื่อง่ายขึ้น
const {Schema} = mongoose


/* ---------------- สร้าง Schema , Class ---------------- */
const roomSchema = Schema({
  name:String,
  capacity:{type:Number,default:50},
  floor:Number,
  //! ref
  building: {type: Schema.Types.ObjectId, ref: 'Building'}
})

module.exports = mongoose.model('Room',roomSchema)