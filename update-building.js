const mongoose = require('mongoose')
const Building = require('./model/Building')
const Room = require('./model/Room')

//! connect
mongoose.connect('mongodb://localhost:27017/example').then(()=>console.log("Connected db:"))


async function main(){
  //!หา oj Building 
  const newInformaticBuilding  = await Building.findById('621b872a174e168a67bb829c')
  //! หา oj Room
  const room = await Room.findById('621b872a174e168a67bb829e')
  //! หา oj Bulding เก่า
  const  informaticBuilding = await Building.findById(room.building)

  room.building = newInformaticBuilding
  //! เพิ่มเข้าตึกใหม่
  newInformaticBuilding.rooms.push(room)
  //! ลบออกจากตึกเก่า
  informaticBuilding.rooms.pull(room)

  newInformaticBuilding.save()
  room.save()
  informaticBuilding.save()
  

}

main().then(()=>console.log('Finish'))