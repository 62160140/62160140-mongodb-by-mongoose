const mongoose = require('mongoose')
const Building = require('./model/Building')
const Room = require('./model/Room')

//! connect
mongoose.connect('mongodb://localhost:27017/example').then(()=>console.log("Connected db:"))


async function main(){
  //TODO FIND
  //! find ได้ array
  // const rooms = await Room.find({capacity:{$gt: 100}})
  //!  find by ID ได้ object 
  // const roomFinded = await Room.findById('621b872a174e168a67bb829d')  
  // console.log(rooms)
  //! findOne ได้ object
  // const roomFindOne = await Room.findOne({name:'3c01'})
  // console.log(roomFindOne)
  //! find แบบดึง object ตัว ref มาแสดงด้วย ใช้ populate
  //  const roomFindOneWithPopuplate = await Room.findOne({name:'4c01'}).populate('building')
  // console.log(roomFindOneWithPopuplate)
  const buildings = await Building.find({}).populate('rooms')
  console.log(JSON.stringify(buildings))
  

  //TODO UPDATE
  // roomFinded.capacity = 20  // !แก้ไข capacity 
  // roomFinded.save()

  //TODO DELETE หาก่อน=>ลบ
  // await Room.findByIdAndDelete('621b872a174e168a67bb829d')

  

}

main().then(()=>console.log('Finish'))