//! ไฟล์ Test

//todo import 
const Room = require('./model/Room')
const Building = require('./model/Building')

const mongoose = require('mongoose')
//! connect db
mongoose.connect('mongodb://localhost:27017/example')


//! aysnc 
async function main(){
  clearDb()
  //* สร้างตึก
  const informaticBuilding = new Building({name:'Informatic',floor:11})
  const newInformaticBuilding = new Building({name:'NewInformatic',floor:20})


  //* สร้าง room
  const room3c01 = await new Room({name:'3c01',capacity:200,floor:3,building:informaticBuilding})
  const room4c01 = await new Room({name:'4c01',capacity:200,floor:4,building:informaticBuilding})
  const room5c01 = await new Room({name:'5c01',capacity:200,floor:5,building:informaticBuilding})



  //todo เพิ่มห้องไปในตึก
  informaticBuilding.rooms.push(room3c01)
  informaticBuilding.rooms.push(room4c01)
  informaticBuilding.rooms.push(room5c01)


  //todo Save ต้องใส่ await ด้วย
  await room3c01.save()
  await room4c01.save()
  await room5c01.save()
  await informaticBuilding.save()
  await  newInformaticBuilding.save()
}

async function clearDb(){
  //! deleteMany หายหมดทั้ง collection
  await Room.deleteMany({}) //! ลบทุกอย่างที่อยู่ใน room
  await Building.deleteMany({}) //! ลบทุกอย่างที่อยู่ใน Building

}

main().then(()=>console.log('Finish'))